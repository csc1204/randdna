#include <string>
#include <random>

using std::string;

string randDNA(int s, string b, int n)
{
	string hold;
	std::mt19937 eng(s);
	std::uniform_int_distribution<int> un(0, b.size()-1);
	
	for(int i=0; i<n; i++)
	{
		int random = un(eng);
		hold += b[random];
	}

return hold;
}
